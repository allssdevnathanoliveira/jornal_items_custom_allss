# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': 'Account - Custom',
    'version': '12.0.1.0.0',
    'category': 'Product',
    'author': 'Nathan Oliveira (ALLSS Soluções em Sistemas)',
    'summary': 'Mask for account',
    'description':'',
    'depends': [
        'account'
    ],
    'data': [
    ],
    'demo': [
    ],
    'css': [],
    'installable': True,
    'auto_install': False,
    'application': False,
}