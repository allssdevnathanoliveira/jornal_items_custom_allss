from odoo import fields, models, api

class AccountMask(models.Model):
    _inherit = 'account.account'

    @api.onchange('code')
    def mask(self):
        cd = self.code

        if cd:
            cd = cd.replace('.', '')

            if len(cd) >= 13:
                code_mask = (f'{cd[0]}.{cd[1]}.{cd[2]}.{cd[3:5]}.{cd[5:7]}.{cd[7:]}')

            elif len(cd) == 2:
                code_mask = (f'{cd[0]}.{cd[1]}')

            elif len(cd) == 3:
                code_mask = (f'{cd[0]}.{cd[1]}.{cd[2]}')

            elif len(cd) == 5:
                code_mask = (f'{cd[0]}.{cd[1]}.{cd[2]}.{cd[3:]}')

            elif len(cd) == 7:
                code_mask = (f'{cd[0]}.{cd[1]}.{cd[2]}.{cd[3:5]}.{cd[5:]}')
            else:
                code_mask = cd

            self.update({
                'code': code_mask
            })