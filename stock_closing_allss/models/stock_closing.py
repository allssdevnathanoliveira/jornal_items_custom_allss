from odoo import fields, models, api, _
from psycopg2 import OperationalError, Error
from odoo.exceptions import UserError, ValidationError
import logging

_logger = logging.getLogger(__name__)

class StockClosing(models.Model):
    _name = 'stock.closing'
    _description = 'ALLSS stock closing'
    _order = '_allss_date_close, _allss_product'

    _allss_date_close = fields.Datetime(string="Data do Fechamento", store=True, help="Escolha uma data para fazer o fechamento de estoque.", default=fields.Datetime.now)
    #_allss_last_closure = fields.Date("Último fechamento:")
    _allss_product = fields.Many2one('product.product', string="Produto", store=True)
    _allss_qty = fields.Float(string="Quantidade", store=True)
    _allss_cost = fields.Float(string="Custo Total", store=True)
    _allss_cost_unit = fields.Float(string="Custo Unitário", store=True)

    @api.multi
    def process_close(self):
        print('ENTROU NA FUNÇÃO AMIGÃO!!!')
        precision_digits = max(6, self.env.ref('product.decimal_product_uom').digits * 2)
        params = (precision_digits, precision_digits)

        UserError(_('ENTROU NA FUNÇÃO AMIGÃO!!!'))
        query = """
                    SELECT date_ant
                         , producty
                         , quantity_ant
                         , cost_ant
                         , SUM(quantity) as quantity
                         , SUM(cost) as cost
                    FROM (
                            SELECT mv.product_id as producty
                                , (mv.product_uom_qty * (CASE
                                                            WHEN (SELECT loc.usage
                                                                FROM stock_location as loc
                                                                WHERE loc.location_id = mv.location_id
                                                                LIMIT 1
                                                                ) = 'internal'
                                                                OR
                                                                (SELECT loc.usage
                                                                FROM stock_location as loc
                                                                WHERE loc.location_id = mv.location_dest_id
                                                                LIMIT 1
                                                                ) <> 'internal'
                                                            THEN  1
                                                            ELSE -1
                                                        END)
                                    ) as quantity
                                , (mv.price_unit * mv.product_uom_qty) as cost
                                , sc._allss_date_close as date_ant
                                , COALESCE(sc._allss_qty,0) as quantity_ant
                                , COALESCE(sc._allss_cost,0) as cost_ant
                                , COALESCE(sc._allss_cost_unit,0) as unit_cost_ant
                            FROM stock_move as mv
                                LEFT JOIN stock_closing as sc ON sc._allss_product = mv.product_id
                                                                AND sc._allss_date_close = ( 
                                                                                            SELECT max(_allss_date_close) 
                                                                                            FROM stock_closing as fs 
                                                                                            WHERE fs._allss_date_close < %s
                                                                                            AND fs._allss_product = sc._allss_product
                                                                                            )

                            WHERE mv.date > (SELECT max(_allss_date_close) 
                                            FROM stock_closing as fs 
                                            WHERE fs._allss_date_close < %s
                                            AND fs._allss_product = mv.product_id
                                            )
                            AND mv.date <= %s
                        ) as tabtmp
                    GROUP BY date_ant
                            , producty
                            , quantity_ant
                            , cost_ant
        ;""" % (self._allss_date_close, self._allss_date_close, self._allss_date_close)
        UserError(_(f"query: {query}"))
        self.env.cr.execute(query)
        teste = self.cr.env.dictfetchall()
        UserError(_(f"teste: {teste}"))

        # try:
        #     with self.env.cr.savepoint():
        #     cr.execute(query, params)
        #     for quant in cr.dictfetchall():
        #         #'_allss_date_close' : quant.date_actual,
        #         self._allss_product : quant['producty'],
        #         self._allss_qty : quant['quantity_ant'] + quant['quantity'],
        #         self._allss_cost : quant['cost_ant'] + quant['cost'],
        #         self._allss_cost_unit : (quant['cost_ant'] + quant['cost']) / (quant['quantity_ant'] + quant['quantity'])
        # except Error as e:
        #     _logger.info('Deu erro colega: %s', e.pgerror)


        tree_view_id = self.env.ref('stock_closing_allss.view_stock_closing_tree').id
        action = {
            'type': 'ir.actions.act_window',
            'views': [(tree_view_id, 'tree')],
            'view_mode': 'tree',
            'name': _(f'Fechamento {self._allss_date_close}'),
            'res_model': 'stock.closing',
            'context': dict(self.env.context, to_date=self._allss_date_close)
        }

        return action