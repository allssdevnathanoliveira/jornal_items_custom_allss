# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': 'Stock Closing',
    'version': '12.0.1.0.0',
    'category': 'Stock',
    'author': 'Nathan Oliveira (ALLSS Soluções em Sistemas)',
    'summary': '',
    'description':'Stock Closing',
    'depends': [
        'stock'
    ],
    'data': [
        #views
        'views/stock_closing_form_init.xml',
        'views/closing.xml'
    ],
    'demo': [
    ],
    'css': [],
    'installable': True,
    'auto_install': False,
    'application': False,
}